const attendanceModel = require('../models/attendance_emp.model');
const JOI = require('joi');
const schema = JOI.object().keys({
    aid: JOI.number().integer(),
    attendance_type: JOI.number().integer(),
    attendance_date: JOI.string().trim(),
    attendance_time: JOI.string().trim(),
    employee_id: JOI.number().integer()
})
    
const validateData = (req)=>{
    const result =  schema.validate(req)
    return result
}

// get all attendance list
exports.getEmployeeAttendance = (req, res)=> {
    //console.log('here all attendance list');
    attendanceModel.getEmployeeAttendance((err, att) =>{
        console.log('We are here');
        if(att.length==0){
            res.json({status:false,message:"No entries found!"})
        }
        if(err)
        res.send(err);
        console.log('Attendance', att);
        res.json({status:true,data:att})
    })
}

// get attendance by ID
exports.getAttendanceByID = (req, res)=>{
    console.log(req.params.id,req.params.date)
    console.log('get attendance by id');
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
    attendanceModel.getAttendanceByID(req.params.id, (err, att)=>{
        console.log(att.length)
        if(att.length==0){
            res.send({success:false,error:"Id does not exist"})
        }else{
        if(err)
        res.send(err);
        console.log('Employee attendance',att);
        res.json({status:true, data:att});
    }})
}


// create new attendance record
exports.createAttendance = (req, res) =>{
    const attendanceReqData = new attendanceModel(req.body);
    console.log('attendanceReqData', attendanceReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        attendanceModel.createNewAttendance(attendanceReqData, (err, att)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Attendance Created Successfully', data: att.insertId})
        })
    }
}

// update attendance
exports.updateAttendance =  (req, res)=>{
    const validationResult = validateData(req.body)
    console.log(validationResult.error)
    if(!validationResult.error){
        
    const attendanceReqData = new attendanceModel(req.body);
    console.log('attendanceReqData update', attendanceReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        attendanceModel.updateAttendance(req.params.id, attendanceReqData, (err, exp)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'attendance updated Successfully'})
        })
    }
}else{
    res.json({status:fail,message:"Something went wrong"})
}
}


// delete attendance
exports.deleteAttendance = (req, res)=>{
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
        attendanceModel.getAttendanceByID(val,(err,rec)=>{
            if(err)
            res.send(err)
            if(rec.length==0)
                res.json({status:"false",message:"Record not found!"})
            else{
    attendanceModel.deleteAttendance(req.params.id, (err, att)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'attendance deleted successfully!'});
    })}
})}
}}

exports.createNewAtt =   (req, res) =>{
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0)
    res.send(400).send({success: false, message: 'Please fill all fields'})
    else{
        const validationResult = validateData(req.body)
        console.log(validationResult.error)
        if(!validationResult.error){
            attendanceModel.getAttendanceByID(req.body.aid,(err,rec)=>{
            if(err)
            res.send(err)
        if(rec.length==0){
            const attendanceReqData = new attendanceModel(req.body);
            console.log('attendanceReqData', attendanceReqData);
            attendanceModel.createNewAttendance(attendanceReqData, (err, exp)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Attendance Created Successfully', data: exp.insertId})
        })
        }
    else{
        res.json({status:false,message:"Record exists"})
    }})
    
}
else{
 res.json({status:false,message:validationResult.error.message})       
}
}}