const expencesModel = require('../models/expences.model');
const JOI = require('joi');
const dbConn = require('../../config/db.config');

const schema = JOI.object().keys({
    eid: JOI.number().integer(),
    expence_employees_id: JOI.number().integer(),
    expence_amount: JOI.string().trim(),
    expence_reason: JOI.string().trim()
})

const validateData = (req)=>{
    const result =  schema.validate(req)
    return result
}

exports.getExpences = (req, res)=> {
    //console.log('here all expences list');
    expencesModel.getExpences((err, exp) =>{
        console.log('We are here');
        if(err)
        res.send(err);
        if(exp.length==0){
            res.json({status:false,message:"No record found!"})
        }
        else{
        console.log('Expences', exp);
        res.json({status:true, data:exp})
        }
    })
}

// get expences by ID
exports.getExpencesByID = (req, res)=>{
    //console.log('get expences by id');
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
    expencesModel.getExpencesByID(req.params.id, (err, exp)=>{
        if(err)
        res.send(err);
        if(exp.length==0)
        res.json({success:false,message:"No record found"});
        else{
        console.log('Employee expences',exp);
        res.json({status:true,data:exp});
        }
    })
}}


// create new expences record
exports.createNewExpences =   (req, res) =>{
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0)
    res.send(400).send({success: false, message: 'Please fill all fields'})
    else{
        const validationResult = validateData(req.body)
        console.log(validationResult.error)
        if(!validationResult.error){
        expencesModel.getExpencesByID(req.body.eid,(err,rec)=>{
            if(err)
            res.send(err)
        if(rec.length==0){
            const expencesReqData = new expencesModel(req.body);
            console.log('expencesReqData', expencesReqData);
            expencesModel.createNewExpences(expencesReqData, (err, exp)=>{
                if(err)
                res.send(err);
                res.json({status: true, message: 'expences Created Successfully', data: exp.insertId})
            })
        }
        else{
        res.json({status:false,message:"Record exists"})
    }})
}
else{
 res.json({status:false,message:validationResult.error.message})       
}
}}

// update expences
exports.updateExpences = (req, res)=>{

    const validationResult = validateData(req.body)
    console.log(validationResult.error)
    if(!validationResult.error){
        
    const expencesReqData = new expencesModel(req.body);
    console.log('expencesReqData update', expencesReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        expencesModel.updateExpences(req.params.id, expencesReqData, (err, exp)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'expences updated Successfully'})
        })
    }
}else{
    res.json({status:fail,message:"Something went wrong"})
}
}


// delete expences
exports.deleteExpences = (req, res)=>{
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
    expencesModel.getExpencesByID(val,(err,rec)=>{
        if(err)
        res.send(err)
        if(rec.length==0)
            res.json({status:"false",message:"Record not found!"})
        else{
            expencesModel.deleteExpences(val, (err, exp)=>{
                if(err)
                res.send(err);
                res.json({success:true, message: 'expences deleted successfully!'});
            })
        }
    })
    
}}