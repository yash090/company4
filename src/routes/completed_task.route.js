//completed_task
const express = require('express');
const router = express.Router();

const completed_tasksController = require('../controllers/completed_task.controller');

// get all completed_task
router.get('/', completed_tasksController.completed_tasksList);

// get completed_task by ID
router.get('/:id',completed_tasksController.getcompleted_taskByID);

// create new completed_task
router.post('/', completed_tasksController.createNewcompleted_task);

// update completed_task
router.put('/:id', completed_tasksController.updatecompleted_task);

// delete completed_task
router.delete('/:id',completed_tasksController.deletecompleted_task);

module.exports = router;