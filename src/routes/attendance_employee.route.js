const express = require('express');
const router = express.Router();

const attendanceController = require('../controllers/attendance_emp.controller');

// get attendance
router.get('/', attendanceController.getEmployeeAttendance);

// get attendance by ID
router.get('/:id/:date',attendanceController.getAttendanceByID);

// create new attendance record
router.post('/', attendanceController.createNewAtt);

// update attendance
// router.put('/:id',attendanceController.updateAttendance);

// delete attendance
// router.delete('/:id', attendanceController.deleteAttendance);

module.exports = router;